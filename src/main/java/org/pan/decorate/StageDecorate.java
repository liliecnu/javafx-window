package org.pan.decorate;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * javafx 窗口装饰器
 *
 * 使用方法:
 * StageDecorate.decorate(primaryStage,root);
 * 具体请见 StageDecorateTest
 * Created by xiaopan on 2016-01-30.
 */
public class StageDecorate {

    public static void decorate(Stage stage, Parent parent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(StageDecorate.class.getResource("stage.fxml"));
            fxmlLoader.load();

            StageController controller = fxmlLoader.getController();
            controller.setStage(stage);
            controller.addContent(parent);

            Scene scene = new Scene(fxmlLoader.getRoot());
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            stage.initStyle(StageStyle.TRANSPARENT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
