package org.pan.alert;

import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.pan.decorate.StageDecorate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 弹出窗
 * Created by Administrator on 2016/2/2.
 */
public class Alerts {

    private AlertType alertType;
    private Image icon;
    private String title;
    private String message;
    private Stage ower;

    private ButtonType resultType;

    private List<ButtonType> buttonTypeList = new ArrayList<>();

    private Alerts(AlertType alertType){
        this.alertType = alertType;

        switch (this.alertType){
            case INFO:
                this.buttonTypeList.add(ButtonType.yes);
                this.icon = new Image(Alerts.class.getResourceAsStream("info_24.png"));
                break;
            case WARN:
                this.buttonTypeList.add(ButtonType.yes);
                this.icon = new Image(Alerts.class.getResourceAsStream("warn_24.png"));
                break;
            case ERROR:
                this.buttonTypeList.add(ButtonType.yes);
                this.icon = new Image(Alerts.class.getResourceAsStream("error_24.png"));
                break;
            case CONFIRM:
                this.buttonTypeList.add(ButtonType.yes);
                this.buttonTypeList.add(ButtonType.no);
                this.icon = new Image(Alerts.class.getResourceAsStream("confirm_24.png"));
                break;
            default:
                break;
        }
    };

    public static Alerts create(AlertType alertType){
        return new Alerts(alertType);
    }

    public Alerts title(String title) {
        this.title = title;
        return this;
    }

    public Alerts message(String message) {
        this.message = message;
        return this;
    }

    public Alerts icon(Image icon) {
        this.icon = icon;
        return this;
    }

    public Alerts ower(Stage ower) {
        this.ower = ower;
        return this;
    }

    public void show(){
        createStage().show();
    }

    public Optional<ButtonType> showAndWait(){
        createStage().showAndWait();
        if (this.resultType == null) {
            return Optional.empty();
        }
        return Optional.of(resultType);
    }

    private Stage createStage(){
        final Stage stage = new Stage();
        if (ower != null) {
            stage.initOwner(this.ower);
        }
        if (title != null) {
            stage.setTitle(this.title);
        }

        HBox hBox_content = new HBox(15);
        hBox_content.setPadding(new Insets(10,10,10,10));
        hBox_content.setAlignment(Pos.CENTER_LEFT);
        ImageView imageView = new ImageView(this.icon);
        Label label = new Label(this.message);
        hBox_content.getChildren().addAll(imageView,label);

        HBox hBox_button = new HBox(15);
        hBox_button.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        hBox_button.setPadding(new Insets(0,10,10,10));
        for (ButtonType buttonType : this.buttonTypeList) {
            Button e = new Button(buttonType.getText());
            e.setUserData(buttonType);
            e.setOnAction(event -> stage.close());
            hBox_button.getChildren().add(e);
        }

        VBox vBox = new VBox(0,hBox_content, hBox_button);

        StageDecorate.decorate(stage,vBox);
        return stage;
    }

}
